import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { IParsedPage } from 'src/app/models/ParsedPage';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html'
})
export class PopupComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public parsedPage: IParsedPage) {}
}
