import { Component, OnInit } from '@angular/core';
import { RssFeedService } from 'src/app/services/rss-feed.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-card-deck',
  templateUrl: './card-deck.component.html'
})

export class CardDeckComponent implements OnInit {

  public feed: Observable<any>;
  private feedUrl = environment.feedSource;

  constructor (private rssFeedService: RssFeedService) {
  }

  ngOnInit() {
    this.getFeed(30);
  }

  private getFeed(count?: number) {
    this.feed = this.rssFeedService.getFeed(this.feedUrl, count);
  }

}
