import { Component, OnInit, Input } from '@angular/core';
import { WebParserService } from 'src/app/services/web-parser.service';
import { MatDialog } from '@angular/material';
import { PopupComponent } from '../popup/popup.component';
import { IParsedPage } from 'src/app/models/ParsedPage';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  constructor (private webParser: WebParserService,
               public dialog: MatDialog) {
  }

  @Input() feedElement;
  private pageUrl: string;
  public imgUrl: string;
  public description: string;
  private parsedPage: IParsedPage;

  ngOnInit() {
    this.pageUrl = this.feedElement.link;
    this.imgUrl = this.feedElement.enclosure.link;
    this.description = this.unescape(this.feedElement.content);
    this.webParser.getParsed(this.pageUrl)
      .subscribe(data => this.parsedPage = data);
  }

  public openPopup() {
    this.dialog.open(PopupComponent, {
      data: this.parsedPage,
      maxWidth: '100vw'
      });
  }

  public unescape(html: string): string {
    const txt = document.createElement('textarea');
    txt.innerHTML = html;
    return txt.value;
  }
}
