import { IFeedElement } from './FeedElement';

export interface IFeed {
  items: IFeedElement[];
}
