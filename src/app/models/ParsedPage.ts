import { Url } from 'url';

export interface IParsedPage {
author: string;
content: string;
date_publised: string;
dek: string;
direction: string;
domain: string;
excerpt: string;
lead_image_url: string;
next_page_url: string;
rendered_pages: number;
title: string;
url: Url;
word_count: number;
}
