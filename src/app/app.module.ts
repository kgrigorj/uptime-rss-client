import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CardComponent } from './components/card/card.component';
import { CardDeckComponent } from './components/card-deck/card-deck.component';
import { MatCardModule, MatDialogModule, MatButtonModule } from '@angular/material';
import { PopupComponent } from './components/popup/popup.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    CardComponent,
    CardDeckComponent,
    PopupComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule
  ],
  entryComponents: [
    PopupComponent
  ],
  providers: [],
  bootstrap: [CardDeckComponent]
})
export class AppModule { }
