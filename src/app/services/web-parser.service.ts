import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IParsedPage } from '../models/ParsedPage';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WebParserService {
  private serviceUrl = environment.parserService.url;
  private key = environment.parserService.key;

  constructor(private http: HttpClient) {
  }

  getParsed(pageUrl: string): Observable<IParsedPage> {
    const headers = new HttpHeaders().set('x-api-key', this.key);
    return this.http.get<IParsedPage>(this.serviceUrl + pageUrl, {headers});
  }
}
