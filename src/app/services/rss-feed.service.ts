import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RssFeedService {
  private serviceUrl = environment.feedService.url;
  private serviceKey = environment.feedService.key;

  constructor(private http: HttpClient) {
  }

  getFeed(feedUrl: string, count?: number): Observable<any> {
    if (count == null || count === 0 || count > 1000) {
      count = 10; // default service value
    }

    const params =  new HttpParams()
      .set('rss_url', feedUrl)
      .set('api_key', this.serviceKey)
      .set('count', count.toString());

    return this.http.get<any>(this.serviceUrl, {params})
      .pipe(map(res => res.items));
  }
}
