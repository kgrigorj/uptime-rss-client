// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  feedSource: 'https://flipboard.com/@raimoseero/feed-nii8kd0sz.rss',
  feedService: {
    url: 'https://api.rss2json.com/v1/api.json',
    key: 'fnnauoaitghpyeitki634ltfgvmynwjychobd0pc'
  },
  parserService: {
    url: 'https://mercury.postlight.com/parser?url=',
    key: 'QfyaA1GAsuecrYbf23ZWdSj5NF1JpzKfV9fsHFLp'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
